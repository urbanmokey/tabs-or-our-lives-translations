{
    "head": {
        "title": "Tabs of our Lives - Manage Your Tabs Now."
    },

    "options": {
        "title": "Tabs of our Lives - Options",
        "pageTitle": "Options",
        "settings": {
            "languages": {
                "header": "Languages",
                "description": "Choose your language for the app:",
                "userMessage": "Reload the page to see the changes.",
                "options": {
                    "en": "English",
                    "zh": "Chinese - 中文",
                    "eo": "Esperanto"
                }
            }
        }
    },

    "navigation": {
        "tabList": {
            "buttons": {
                "alt": "tab lists",
                "title": "tab lists"
            }
        },
        "saved": {
            "buttons": {
                "alt": "saved tabs",
                "title": "saved tabs"
            }
        },
        "about": {
            "buttons": {
                "alt": "about extension",
                "title": "about extension"
            }
        },
        "plainText": {
            "buttons": {
                "alt": "URLs in plain text",
                "title": "URLs in plain text"
            }
        }
    },

    "extension": {
        "name": {
            "start": "Tabs",
            "end": "of our lives"
        },
        "author": {
            "url": "https://www.linkedin.com/in/njensen0604/",
            "name": "N Jensen"
        },
        "copyright": {
            "dates": "© 2019 - "
        },
        "overview": {
            "url": "https://chrome.google.com/webstore/detail/tabs-of-our-lives-keep-yo/hpdgkdeokgdljggbajofdlcacgdolopc",
            "text": "Overview"
        },
        "reviews": {
            "url": "https://chrome.google.com/webstore/detail/tabs-of-our-lives-keep-yo/hpdgkdeokgdljggbajofdlcacgdolopc",
            "text": "Feedback"
        },
        "support": {
            "url": "https://chrome.google.com/webstore/detail/tabs-of-our-lives-keep-yo/hpdgkdeokgdljggbajofdlcacgdolopc",
            "text": "Support"
        }
    },

    "windowSession": {
        "recordTabs": {
            "checkError": "Select some tabs to save",
            "initiate": "Saving tabs...",
            "saved": "Tabs saved!",
            "error": "Error occurred saving tabs.",
            "fail": "Unable to save tabs."
        }
    },

    "featurePane": {
        "tabList": {
            "header": "My Tabs",
            "search": {
                "placeholder": "Search"
            },
            "session": {
                "header": "Current Window",
                "headerOther": "Other Window",
                "tabCount": {
                    "singular": " tab",
                    "plural": " tabs"
                },
                "buttons": {
                    "save": {
                        "text": "Save",
                        "title": "Save Tabs",
                        "alt": "Save Tabs"
                    },
                    "close": {
                        "title": "Close Tab",
                        "alt": "Close Tab"
                    }
                }
            }
        },

        "savedTabs": {
            "header": "Saved Tabs",
            "buttons": {
                "restore": {
                    "regular": {
                        "text": "Restore",
                        "title": "Restore Tabs",
                        "alt": "Restore Tabs"
                    },
                    "incognito": {
                        "text": "Restore incognito",
                        "title": "Restore in Incognito",
                        "alt": "Restore in Incognito"
                    }
                },
                "remove": {
                    "text": "Remove",
                    "title": "Remove Group",
                    "alt": "Remove Group",
                    "message": "Tab group removed"
                }
            },
            "tabCount": {
                "singular": " tab",
                "plural": " tabs"
            },
            "selector": {
                "reset": "-- show all --"
            }
        },

        "closedTabs": {
            "header": "Closed Tabs"
        },

        "about": {
            "header": "About Tabs of our Lives",
            "synopsis": "Wrangle in your tabs tonight. Don't lose track of your work. With a few clicks and taps, you'll easily manage your many windows and tabs.",
            "feedback": {
                "header": "Contact Us",
                "anchorText": "Visit the extension home page",
                "text": " to leave a feedback, a rating, or contact support."
            },
            "keyboardShortcut": {
                "header": "Keyboard Shortcuts",
                "text": "<strong>ALT + T</strong> will open the extension popup."
            },
            "translators": {
                "header": "Translators",
                "text": "<a class='anchor' href='https://www.linkedin.com/in/alan-ng-4325316b' target='_blank'>Alan Ng</a> | Chinese",
                "helpUs": "* To help translate into your language, please checkout <a class='anchor' href='https://gitlab.com/urbanmokey/tabs-or-our-lives-translations' target='_blank'>this git-repo</a>."
            },
            "options": {
                "header": "Options",
                "button": {
                    "text1": "Right-click extension icon → ",
                    "text2": "Click 'Options'."
                }
            }
        },

        "plainText": {
            "header": "URLs in Plain Text",
            "buttons": {
                "copyUrls": {
                    "text": "Copy URLs",
                    "title": "Copy URLs",
                    "alt": "Copy URLs"
                }
            },
            "textarea": {
                "placeholder": "Your URLs..."
            },
            "message": "URLs Copied"
        },

        "tabSelection": {
            "header": "Save Tabs Now",
            "buttons": {
                "save": {
                    "text": "Save",
                    "title": "Save Tabs",
                    "alt": "Save Tabs",
                    "userMessage": {
                        "start": "Saving tabs...",
                        "success": "Tabs saved!",
                        "fail": "Could not save tabs. Try again later.",
                        "error": "Something went wrong :/",
                        "nothingSelected": "Select some tabs to save..."
                    }
                },
                "cancel": {
                    "text": "Cancel",
                    "title": "Cancel Tabs",
                    "alt": "Cancel Tabs"
                },
                "pinned": {
                    "text": "Pinned",
                    "title": "Pinned Tabs",
                    "alt": "Pinned Tabs"
                },
                "highlighted": {
                    "text": "Highlighted",
                    "title": "Highlighted Tabs",
                    "alt": "Highlighted Tabs"
                },
                "all": {
                    "text": "All / None",
                    "title": "All or No Tabs",
                    "alt": "All or No Tabs"
                }
            }
        }
    },

    "timeDate": {
        "today": "Today",
        "yesterday": "Yesterday",
        "beforeNoonCode": "AM",
        "afterNoonCode": "PM",
        "month": {
            "code0": "Jan",
            "code1": "Feb",
            "code2": "Mar",
            "code3": "Apr",
            "code4": "May",
            "code5": "Jun",
            "code6": "Jul",
            "code7": "Aug",
            "code8": "Sep",
            "code9": "Oct",
            "code10": "Nov",
            "code11": "Dec"
        },
        "day": {
            "code0": "Sun",
            "code1": "Mon",
            "code2": "Tue",
            "code3": "Wed",
            "code4": "Thu",
            "code5": "Fri",
            "code6": "Sat"
        }
    }
}
