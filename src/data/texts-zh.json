{
    "head": {
        "title": "我們的分頁 - 容易地管理你的分頁。"
    },

    "options": {
        "title": "我們的分頁 - 選項",
        "pageTitle": "選項",
        "settings": {
            "languages": {
                "header": "語言",
                "description": "請選擇擴展程序語言:",
                "userMessage": "請重新載入.",
                "options": {
                    "en": "英語 - English",
                    "zh": "中文",
                    "eo": "世界語 - Esperanto"
                }
            }
        }
    },

    "navigation": {
        "tabList": {
            "buttons": {
                "alt": "分頁列表",
                "title": "分頁列表"
            }
        },
        "saved": {
            "buttons": {
                "alt": "保存分頁",
                "title": "保存分頁"
            }
        },
        "about": {
            "buttons": {
                "alt": "關於擴展程序",
                "title": "關於擴展程序"
            }
        },
        "plainText": {
            "buttons": {
                "alt": "網址纯文本",
                "title": "網址纯文本"
            }
        }
    },

    "extension": {
        "name": {
            "start": "我們的",
            "end": "分頁"
        },
        "author": {
            "url": "https://www.linkedin.com/in/njensen0604/",
            "name": "N Jensen"
        },
        "copyright": {
            "dates": "© 2019 - "
        },
        "overview": {
            "url": "https://chrome.google.com/webstore/detail/tabs-of-our-lives-keep-yo/hpdgkdeokgdljggbajofdlcacgdolopc",
            "text": "概述"
        },
        "reviews": {
            "url": "https://chrome.google.com/webstore/detail/tabs-of-our-lives-keep-yo/hpdgkdeokgdljggbajofdlcacgdolopc",
            "text": "評語"
        },
        "support": {
            "url": "https://chrome.google.com/webstore/detail/tabs-of-our-lives-keep-yo/hpdgkdeokgdljggbajofdlcacgdolopc",
            "text": "幫助"
        }
    },

    "windowSession": {
        "recordTabs": {
            "checkError": "請選擇一些分頁去保存",
            "initiate": "保存分頁中...",
            "saved": "分頁已保存!",
            "error": "保存分頁失誤。",
            "fail": "保存分頁失敗。"
        }
    },

    "featurePane": {
        "tabList": {
            "header": "我的分頁",
            "search": {
                "placeholder": "搜索"
            },
            "session": {
                "header": "現時的視窗",
                "headerOther": "其它視窗",
                "tabCount": {
                    "singular": " 分頁",
                    "plural": " 分頁"
                },
                "buttons": {
                    "save": {
                        "text": "保存",
                        "title": "保存分頁",
                        "alt": "保存分頁"
                    },
                    "close": {
                        "title": "關閉分頁",
                        "alt": "關閉分頁"
                    }
                }
            }
        },

        "savedTabs": {
            "header": "已保存分頁",
            "buttons": {
                "restore": {
                    "regular": {
                        "text": "恢復",
                        "title": "恢復分頁",
                        "alt": "恢復分頁"
                    },
                    "incognito": {
                        "text": "恢復",
                        "title": "恢復分頁",
                        "alt": "恢復分頁"
                    }
                },
                "remove": {
                    "text": "刪除",
                    "title": "刪除小組",
                    "alt": "刪除小組",
                    "message": "小組已刪除"
                }
            },
            "tabCount": {
                "singular": " 分頁",
                "plural": " 分頁"
            },
            "selector": {
                "reset": "-- 顯示全部 --"
            }
        },

        "closedTabs": {
            "header": "已關閉分頁"
        },

        "about": {
            "header": "關於我們的分頁",
            "synopsis": "容易地管理你的分頁, 只需要一按恢復, 你所有的分頁便回來了。",
            "feedback": {
                "header": "聯絡我們",
                "anchorText": "請探訪我們的首頁",
                "text": " 去留言, 評分, 或幫助。"
            },
            "keyboardShortcut": {
                "header": "鍵盤快捷鍵",
                "text": "<strong>ALT + T</strong> 開擴展程序功能。"
            },
            "translators": {
                "header": "翻譯者",
                "text": "<a class='anchor' href='https://www.linkedin.com/in/alan-ng-4325316b' target='_blank'>Alan Ng</a> | 中文",
                "helpUs": "* To help translate into your language, please checkout <a class='anchor' href='https://gitlab.com/urbanmokey/tabs-or-our-lives-translations' target='_blank'>this git-repo</a>."
            },
            "options": {
                "header": "選項",
                "button": {
                    "text1": "右鍵點擊擴展程序圖標 → ",
                    "text2": "點擊 '選項'."
                }
            }
        },

        "plainText": {
            "header": "網址纯文本",
            "buttons": {
                "copyUrls": {
                    "text": "複製網址",
                    "title": "複製網址",
                    "alt": "複製網址"
                }
            },
            "textarea": {
                "placeholder": "你的網址"
            },
            "message": "網址已複製"
        },

        "tabSelection": {
            "header": "現在保存分頁",
            "buttons": {
                "save": {
                    "text": "保存",
                    "title": "保存分頁",
                    "alt": "保存分頁",
                    "userMessage": {
                        "start": "保存分頁中...",
                        "success": "分頁已保存!",
                        "fail": "保存分頁失誤。 請稍後再試。",
                        "error": "有些事失誤 :/",
                        "nothingSelected": "請選擇一些分頁去保存..."
                    }
                },
                "cancel": {
                    "text": "取消",
                    "title": "取消分頁",
                    "alt": "取消分頁"
                },
                "pinned": {
                    "text": "鎖定",
                    "title": "鎖定分頁",
                    "alt": "鎖定分頁"
                },
                "highlighted": {
                    "text": "突出顯示",
                    "title": "突出顯示分頁",
                    "alt": "突出顯示分頁"
                },
                "all": {
                    "text": "全部",
                    "title": "全部分頁",
                    "alt": "全部分頁"
                }
            }
        }
    },

    "timeDate": {
        "today": "今日",
        "yesterday": "昨日",
        "beforeNoonCode": "上午",
        "afterNoonCode": "下午",
        "month": {
            "code0": "一月",
            "code1": "二月",
            "code2": "三月",
            "code3": "四月",
            "code4": "五月",
            "code5": "六月",
            "code6": "七月",
            "code7": "八月",
            "code8": "九月",
            "code9": "十月",
            "code10": "十一月",
            "code11": "十二月"
        },
        "day": {
            "code0": "星期日",
            "code1": "星期一",
            "code2": "星期二",
            "code3": "星期三",
            "code4": "星期四",
            "code5": "星期五",
            "code6": "星期六"
        }
    }
}
