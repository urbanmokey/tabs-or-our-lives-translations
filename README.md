# Tabs of Our Lives - Translations

This repo serves as the translation on-screen text for the extension [Tabs of Our Lives](https://chrome.google.com/webstore/detail/tabs-of-our-lives-manage/hpdgkdeokgdljggbajofdlcacgdolopc/).

## Contributions

Please help with translations in your language! 

Create a pull request with your language translations. If they are accepted, you'll be listed as a translator in the extension. :)

## Development

### Prettier and ESLint

The [Prettier](https://prettier.io/) extension is an opinionated code formatter. It removes all original styling and ensures that all outputted code conforms to a consistent style. 

The [ESLint](https://eslint.org/) extension is an open source JavaScript linting utility. Code linting is a type of static analysis that is frequently used to find problematic patterns or code that doesn't adhere to certain style guidelines. The primary reason ESLint was created was to allow developers to create their own linting rules. ESLint is designed to have all rules completely pluggable. The default rules are written just like any plugin rules would be. They can all follow the same pattern, both for the rules themselves as well as tests. While ESLint will ship with some built-in rules to make it useful from the start, you'll be able to dynamically load rules at any point in time.

#### Setup Prettier and ESLint

These extensions need these NPM packages to work properly: `prettier`, `eslint`, `eslint-config-prettier` `eslint-plugin-prettier`, and `babel-eslint`. If these are not in the `package.json`, you can install them with `npm install prettier eslint eslint-config-prettier eslint-plugin-prettier babel-eslint --save-dev`.

**Initiate ESLint**:

`npx eslint --init`

This will create a new config file for ESLint.

**In VSCode**:
 
Install ESLint by Dirk Baeumer and Prettier by Prettier. They should start working automatically.

**In Webstorm**:

Go to settings, search for ESLint, open ESLint under Code Quality Tools. Select "Manual ESLing configuration". Make sure that the "Node interpreter" and the "ESLint package" are both filled out. Click apply.

Go to settings, search for Prettier, open Prettier under Languages & Frameworks -> JavaScript. Make sure that "Node interpreter" and Prettier package" are filled out. Click apply.